package com.example.tapandtapagain;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomappbar.BottomAppBar;

import java.util.Random;

public class GameActivity extends AppCompatActivity {

    private int pos_x;
    private int pos_y;
    private int max_height;
    private int min_height;
    private int max_width;
    private int level;
    private int tap_limit;
    private int tap;
    private int total_tap;

    private Random rand;

    private ImageView dot;
    private TextView txt_tap;
    private TextView txt_level;
    private TextView txt_final_time;
    private Chronometer chrono;
    private Button btReset;
    private AppBarLayout appBarLayout;
    private BottomAppBar bottomAppBar;

    @Override
    public boolean onTouchEvent(MotionEvent e) {

        if (e.getAction() == MotionEvent.ACTION_DOWN) {
            total_tap++;
        }

        return true;
    }

    /*
    @Override
    public void onBackPressed() {
        // Back button disabled
    }
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.activity_game);

        // Screen Size Information
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height_screen = displayMetrics.heightPixels;
        int width_screen = displayMetrics.widthPixels;

        rand = new Random();

        // Dot attributes
        dot = findViewById(R.id.dot);

        // TextView
        txt_tap = findViewById(R.id.tap);
        txt_level = findViewById(R.id.level);
        txt_final_time = findViewById(R.id.final_time);

        // My chronometer
        chrono = findViewById(R.id.chrono);

        // Button
        btReset = findViewById(R.id.reset);

        // AppBarLayout
        appBarLayout = findViewById(R.id.appBarLayout);
        bottomAppBar = findViewById(R.id.bottomAppBarGame);

        // Limit of dot in screen
        max_height = height_screen - dot.getLayoutParams().height - 5;
        max_width = width_screen - dot.getLayoutParams().width;

        min_height = appBarLayout.getLayoutParams().height;

        dot.setX(randNum(0,max_width)); // Random X - Width
        dot.setY(randNum(min_height, max_height)); // Random Y - Height

        // Call this function for all parameters
        initParam();

        // Reset Button OnClick
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Dot Actions OnClick
        dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos_x = randNum(0,max_width); // Set horizontal value in screen limits
                pos_y = randNum(min_height,max_height); // Set vertical value in screen limits

                // Update Tap number and Level
                if (tap == tap_limit-1) {
                    tap = 0;
                    txt_tap.setText(getString(R.string.tap, tap, tap_limit));
                    level++;
                    txt_level.setText(getString(R.string.level, level));
                } else {
                    tap++;
                    txt_tap.setText(getString(R.string.tap, tap, tap_limit));
                }

                if (level == 11) {
                    level = 10;
                    tap = tap_limit;
                    txt_tap.setText(getString(R.string.tap, tap, tap_limit));
                    chrono.stop();
                    txt_level.setText(getString(R.string.level, level));
                    dot.setVisibility(View.INVISIBLE);
                    btReset.setVisibility(View.VISIBLE);
                    txt_final_time.setVisibility(View.VISIBLE);
                }

                // Increase number of tap
                total_tap++;

                dot.setColorFilter(Color.rgb(randNum(80,255), randNum(80,255), randNum(80,255)));

                txt_final_time.setText(getString(R.string.final_time, chrono.getText(), calculateAccuracy()));

                // Move the dot
                dot.animate().x(pos_x).y(pos_y).setDuration(durationCalculator());
            }
        });
    }

    // Initial params
    public void initParam() {

        // Variables
        level = 1;
        tap_limit = 5; // TODO: must change this value to 30
        tap = 0;
        total_tap = 0;

        // Dot
        dot.setVisibility(View.VISIBLE);
        dot.setColorFilter(Color.rgb(randNum(80,255), randNum(80,255), randNum(80,255)));

        // TextView
        txt_level.setText(getString(R.string.level, level));
        txt_tap.setText(getString(R.string.tap, tap, tap_limit));
        txt_final_time.setVisibility(View.INVISIBLE);

        // Button
        btReset.setVisibility(View.INVISIBLE);

        // Chronometer
        chrono.start();
    }

    // Change duration for speed animation
    public int durationCalculator(){
        int duration = 1000;
        for (int i=1; i<level; i++) {
            duration /= 1.7; // Change the ratio of move speed
        }

        return duration;
    }

    // Select a random number between min and max included [min - max]
    public int randNum(int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }

    // Calculate accuracy
    public double calculateAccuracy() {
        return (double) level*tap_limit/total_tap*100;
    }
}